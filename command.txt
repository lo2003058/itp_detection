# Convert darknet weights to tensorflow
## yolov4
python save_model.py --weights ./data/yolov4.weights --output ./checkpoints/yolov4-416 --input_size 416 --model yolov4 

# yolov4-tiny
python save_model.py --weights ./data/yolov4-tiny.weights --output ./checkpoints/yolov4-tiny-416 --input_size 416 --model yolov4 --tiny

# custom yolov4
python save_model.py --weights ./data/custom.weights --output ./checkpoints/custom-416 --input_size 416 --model yolov4 

# Run yolov4 tensorflow model
python detect.py --weights ./checkpoints/yolov4-416 --size 416 --model yolov4 --images ./data/images/kite.jpg

# Run yolov4-tiny tensorflow model
python detect.py --weights ./checkpoints/yolov4-tiny-416 --size 416 --model yolov4 --images ./data/images/kite.jpg --tiny

# Run custom yolov4 tensorflow model
python detect.py --weights ./checkpoints/custom-416 --size 416 --model yolov4 --images ./data/images/car.jpg

# Run yolov4 on video
python detect_video.py --weights ./checkpoints/yolov4-416 --size 416 --model yolov4 --video ./data/video/video.mp4 --output ./detections/results.avi

# Run custom yolov4 model on video
python detect_video.py --weights ./checkpoints/custom-416 --size 416 --model yolov4 --video ./data/video/cars.mp4 --output ./detections/results.avi

# Run yolov4 on webcam
python detect_video.py --weights ./checkpoints/yolov4-416 --size 416 --model yolov4 --video 0

custom
python detect_video.py --weights ./checkpoints/custom_tiny_best20220309 --size 416 --model yolov4 --video 0 --tiny --send_data

============================================================================================================================================================

python save_model.py --weights ./data/custom_tiny_best20220309.weights --output ./checkpoints/custom_tiny_best20220309 --input_size 416 --model yolov4 --tiny

python detect.py --weights ./checkpoints/custom-best-tiny-416 --size 416 --model yolov4 --images ./data/images/test.png --tiny

python detect_video.py --weights ./checkpoints/custom_tiny_best20220309 --size 416 --model yolov4 --video 1 --output ./detections/results20220309.mp4 --tiny

python detect_video.py --weights ./checkpoints/custom_tiny_best20220309 --size 416 --model yolov4 --video ./data/video/office.mp4 --output ./detections/office.mp4 --tiny

