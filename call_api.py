from threading import Thread
from time import sleep
from datetime import datetime, timedelta


def call_api():
    print('send')


def schedule_api():
    while datetime.now().minute % 1 != 0:
        sleep(1)
    call_api()
    while True:
        sleep(60)
        call_api()


def start():
    thread = Thread(target=schedule_api, daemon=True)
    thread.daemon = True
    thread.start()


thread = Thread(target=schedule_api, daemon=True)
thread.start()
