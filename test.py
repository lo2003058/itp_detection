import requests


def call_api(class_dict):
    # 將資料加入 POST 請求中
    requests.post('http://localhost:8000/api/receiveMaskData', data=class_dict)


# 資料
my_data = {'key1': 'value1', 'key2': 'value2'}
call_api(my_data)
